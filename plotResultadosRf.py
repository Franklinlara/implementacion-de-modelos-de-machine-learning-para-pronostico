import matplotlib.pyplot as plt
x = [2,10,100,250,500]
PCC = [0.9997,0.9998,0.9998,0.9998,0.9998]
NSE = [0.9995,0.9996,0.9996,0.9996,0.9996]
RMSE = [0.173,0.164,0.161,0.161,0.167]
BIAS = [-0.0000015,0.0000000757,-0.00000239,-0.00000131,0.0]
a = [97.88,99.74,99.75,99.79,99.49]

fig, axs = plt.subplots(2, 2)
plt.title("Valores de criterios de evaluación en los modelos de RF")
axs[0, 0].plot(x, PCC)
axs[0, 0].set_title('PCC')
axs[0, 1].plot(x, NSE)
axs[0, 1].set_title('NSE')
axs[1, 0].plot(x, RMSE)
axs[1, 0].set_title('RMSE')
axs[1, 1].plot(x, BIAS)
axs[1, 1].set_title('BIAS')
plt.show()
plt.clf()

plt.plot(x,a)
plt.xlabel("Número de árboles en RF")
plt.ylabel("Accuracy (Exactitud)")
plt.title("Accuracy por número de árboles en los entrenamientos de RF")
plt.show()

plt.show()