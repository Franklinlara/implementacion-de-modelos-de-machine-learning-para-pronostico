from sklearn.ensemble import RandomForestRegressor
import numpy as np 
import matplotlib.pyplot as plt 
import pandas as pd 
import datetime
from sklearn import model_selection
from sklearn.linear_model import LogisticRegression
import pickle
from datetime import datetime as dt
from sklearn.datasets import make_regression
from sklearn.model_selection import train_test_split
from scipy.io import arff
from sklearn import preprocessing
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import r2_score
from sklearn.model_selection import RandomizedSearchCV
from scipy.stats import linregress
from sklearn.metrics import accuracy_score
from sklearn.metrics import mean_squared_error


### hyperparametros

def dectree_max_depth(tree):
    n_nodes = tree.node_count
    children_left = tree.children_left
    children_right = tree.children_right

    def walk(node_id):
        if (children_left[node_id] != children_right[node_id]):
            left_max = 1 + walk(children_left[node_id])
            right_max = 1 + walk(children_right[node_id])
            return max(left_max, right_max)
        else: # leaf
            return 1

    root_node_id = 0
    return walk(root_node_id)

def evaluate(model, test_features, test_labels):
    predictions = model.predict(test_features)
    errors = abs(predictions - test_labels)
    mape = 100 * np.mean(errors / test_labels)
    accuracy = 100 - mape
    print('Model Performance')
    print('Average Error: {:0.4f} degrees.'.format(np.mean(errors)))
    print('Accuracy = {:0.2f}%.'.format(accuracy))
    
    return accuracy

n_estimators = [int(x) for x in np.linspace(start = 1, stop = 10, num = 2)]
max_features = ['auto', 'sqrt']
max_depth = [int(x) for x in np.linspace(1, 10, num = 2)]
max_depth.append(None)
min_samples_split = [2, 5, 10]
min_samples_leaf = [1, 2, 4]
bootstrap = [True, False]

random_grid = {'n_estimators': n_estimators,
               'max_features': max_features,
               'max_depth': max_depth,
               'min_samples_split': min_samples_split,
               'min_samples_leaf': min_samples_leaf,
               'bootstrap': bootstrap}


instanteInicial = dt.now()
print("Recoleccion de datos")

data = pd.read_csv("DataTraining.csv")

uno = data["0"]

data = pd.DataFrame(data)

del data['Unnamed: 0']

standardScaler_x = StandardScaler()
data = standardScaler_x.fit_transform(data)

print("Data normalizado")
data = pd.DataFrame(data)

entrenamiento = data[data.columns[0:14]]
salida = data[data.columns[14]]

salida2 = data[data.columns[14]]

x, x_test, y, y_test = train_test_split(entrenamiento, salida , test_size=0.2)

print("Datos para entrenamiento")
print(len(y))
print("Datos para validar")
print(len(y_test))

pd.DataFrame(y_test).to_csv('y_testRF02Real.csv')

print("Modelos Aprendiendo")
regresion = RandomForestRegressor(n_estimators=1000, n_jobs=-1)

regresion.fit(x, y)

print("regresion")
print(regresion)

print("Validacion")
y_pred = regresion.predict(x_test)

print("Almacenando datos normalizados")

np.savetxt('Normalizados1RF01Pred.csv', np.array(y_pred), delimiter=',')
np.savetxt('NormalizadosRF01Real.csv', np.array(y_test), delimiter=',')

print("Error medio Cuadratico Lineal")
print(mean_squared_error(y_test, y_pred))

print("NSE: ")
print(r2_score(y_test, y_pred))

print("PCC")
print(linregress(y_pred, y_test))
slope, intercept, r_value, p_value, std_err = linregress(y_test, y_pred)
print("slope: %f    intercept: %f" % (slope, intercept))
plt.plot(y_test, y_pred, 'o', label='Datos Originales')
plt.plot(y_test, intercept + slope*y_test, 'r', label='Ajuste')
plt.legend()
plt.savefig("imagenes/PCCRF02NOR.png", transparent=True, format="png")
plt.clf()

print("Bias")
a = y_pred - y_test
a = sum(a)
b = sum(y_test)
print(a/b)

print("varianza")
print(y_test.var())
print(y_pred.var())
print(y_test.var() - y_pred.var())

# Calculos d elos valores para ver el error de otra forma
errors = abs(y_pred - y_test)

# Calculate mean absolute percentage error (MAPE)
mape = 100 * (errors / abs(y_test))

# Calculate and display accuracy 
accuracy = 100 - np.mean(mape)
print("Acurracy sklearn")
print(regresion.score(x_test, y_test))

print("graficando")
x  = [a for a in range(len(y_pred))]

yNor = (abs(y_pred)  - abs(y_test))[:100000]

### regresando a valores reales
print("regresando a valores reales")
x_test = x_test.reset_index()
x_test2 = x_test.copy()
x_test2 = x_test2.reset_index()

del x_test2['level_0']
del x_test2['index']
del x_test['index']

y_test = y_test.tolist()

x_test['14'] = pd.Series(y_pred)
x_test2['14'] = pd.Series(y_test)

print("valores reales")
datos2 = standardScaler_x.inverse_transform(x_test)
datos2 = pd.DataFrame(datos2)

datos3 = standardScaler_x.inverse_transform(x_test2)
datos3 = pd.DataFrame(datos3)

print("Almacenando Matrices")
pd.DataFrame(datos2).to_csv('FinalMatrixPronosticadaRF02.csv')
pd.DataFrame(datos3).to_csv('FinalMatrixTestRF02.csv')

y_pred =  datos2[datos2.columns[14]]
y_test =  datos3[datos3.columns[14]]

# Calcular valores reales
print("-------Con los valores Reales----------") 
print("Error medio Cuadratico Lineal")
print(mean_squared_error(y_test, y_pred))

print("NSE: ")
print(r2_score(y_test, y_pred))

print("PCC")
print(linregress(y_pred, y_test))
slope, intercept, r_value, p_value, std_err = linregress(y_test, y_pred)
print("slope: %f    intercept: %f" % (slope, intercept))
plt.plot(y_test, y_pred, 'o', label='Datos Originales')
plt.plot(y_test, intercept + slope*y_test, 'r', label='Ajuste')
plt.legend()
plt.savefig("imagenes/PCCRF02REAL.svg", transparent=True, format="svg")
plt.clf()

print("Bias")
a = y_pred - y_test
a = sum(a)
b = sum(y_test)
print(a/b)

print("varianza")
print(y_test.var())
print(y_pred.var())
print(y_test.var() - y_pred.var())
# Calculos d elos valores para ver el error de otra forma
errors = abs(y_pred - y_test)

# Calculate mean absolute percentage error (MAPE)
mape = 100 * (errors / abs(y_test))

# Calculate and display accuracy 
accuracy = 100 - np.mean(mape)

print("Almacenando solo valores de salida") 
pd.DataFrame(y_pred).to_csv('FinalRealy_predRF02.csv')
pd.DataFrame(y_test).to_csv('FinalRealy_testRF02.csv')


# save the model to disk
pickle.dump(regresion, open('FinalPruebaRF02.sav', 'wb'))

# al final de la partida
instanteFinal = dt.now()
tiempo = instanteFinal - instanteInicial # Devuelve un objeto timedelta
segundos = tiempo.seconds
print("Tiempo: "+segundos)

print(list(zip(x, regresion.feature_importances_)))

feature_list = list(entrenamiento.columns)

# Pull out one tree from the forest
# Import tools needed for visualization
from sklearn.tree import export_graphviz
# Pull out one tree from the forest
tree = regresion.estimators_[5]
# Export the image to a dot file
export_graphviz(tree, out_file = 'tree4.dot', feature_names = feature_list, rounded = True, precision = 1)
# Use dot file to create a graph
(graph, ) = pydot.graph_from_dot_file('tree3.dot')
# Write graph to a png file
#graph.write_png('tree.png')


from subprocess import call
call(['dot', '-Tpng', 'tree4.dot', '-o', 'tree.png', '-Gdpi=600'])

# Display in python
import matplotlib.pyplot as plt
plt.figure(figsize = (14, 18))
plt.imshow(plt.imread('tree.png'))
plt.axis('off')
plt.show()

print("Tamanio Arbol")
print([dectree_max_depth(t.tree_) for t in regresion.estimators_])


# conversion z- r
print("conversion z - R")

Z_test = 10**(y_test/10)
Z_pred = 10**(y_pred/10)

convR = Z_test ** 2.03
convR = 103*convR

convT = Z_pred ** 2.03
convT = 103*convT

print("Almacenando solo valores de tasa de lluvia") 
pd.DataFrame(convT).to_csv('TasaLluviaFinalRealy_predRF02.csv')
pd.DataFrame(convR).to_csv('TasaLluviaFinalRealy_testRF02.csv')

# Calcular valores reales
print("-------Con los valores Tasa de lluvia----------") 

print("NSE: ")
print(r2_score(convR, convT))

print("PCC")
print(linregress(convT, convR))
slope, intercept, r_value, p_value, std_err = linregress(convR, convT)
print("slope: %f    intercept: %f" % (slope, intercept))
plt.plot(convR, convT, 'o', label='Datos Originales')
plt.plot(convR, intercept + slope*convR, 'r', label='Ajuste')
plt.legend()
plt.savefig("imagenes/PCCRF02TL.svg", transparent=True, format="svg")
plt.clf()

print("varianza")
print(y_test.var())
print(y_pred.var())
print(y_test.var() - y_pred.var())

print("Bias")
a = convT - convR
a = sum(a)
b = sum(convR)
print(a/b)

# Calculos d elos valores para ver el error de otra forma
errors = abs(convT - convR)

# Print out the mean absolute error (mae)
print('Mean Absolute Error:', round(np.mean(errors), 5), 'degrees.')

# Calculate mean absolute percentage error (MAPE)
mape = 100 * (errors / abs(convR))

# Calculate and display accuracy 
accuracy = 100 - np.mean(mape)

features = pd.get_dummies(data)
feature_list = list(features.columns)

# Get numerical feature importances
importances = list(regresion.feature_importances_)

# List of tuples with variable and importance
feature_importances = [(feature, round(importance, 2)) for feature, importance in zip(feature_list, importances)]

# Sort the feature importances by most important first
feature_importances = sorted(feature_importances, key = lambda x: x[1], reverse = True)

# Print out the feature and importances 
[print('Variable: {:20} Importancia: {}'.format(*pair)) for pair in feature_importances];

plt.clf()

# Set the style
plt.style.use('fivethirtyeight')
# list of x locations for plotting
x_values = list(range(len(importances)))
# Make a bar chart
plt.bar(x_values, importances, orientation = 'vertical')
# Tick labels for x axis
plt.xticks(x_values, feature_list, rotation='vertical')
# Axis labels and title
plt.ylabel('Importancia'); plt.xlabel('Variable'); plt.title('Importancia de las Variables');
plt.savefig("imagenes/IVRF02TL.svg", transparent=True, format="svg")
plt.clf()