import numpy as np 
import matplotlib.pyplot as plt 
import pandas as pd 
import datetime
import pickle
from datetime import datetime as dt
from scipy.io import arff

print("Recoleccion de datos")

data = pd.read_csv("Entrenamiento.csv")

size = len(data) 
d = {}

x = data['0'].tolist()
y = data['1'].tolist()
des = data['3'].tolist()
media = data['4'].tolist()

for i in range(size):
    if (d.get(str(x[i])+str(y[i])) == None):
        d.update({str(x[i])+str(y[i]):[[x[i], y[i], des[i], media[i]]]})
    else:
        lista = []
        lista = d.get(str(x[i])+str(y[i]))
        lista.append([x[i], y[i], des[i], media[i]])
        d.update({str(x[i])+str(y[i]):lista})
 
matriz = []

for i in d.values():
    aux = []
    for n in range(6, 4744):
        matriz.append([i[n][0], i[n][1], i[n-6][2], i[n-6][3], i[n-5][2], i[n-5][3],
        i[n-4][2], i[n-4][3], i[n-3][2], i[n-3][3], i[n-2][2], i[n-2][3],
        i[n-1][2], i[n-1][3], i[n][3]])

pd.DataFrame(matriz).to_csv('DataTraining.csv')
print("Finalizado")