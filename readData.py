import netCDF4
import pandas as pd
import matplotlib.pyplot as plt
import datetime as dt
import os
import numpy as np

total = 0
archivos = 0
matriz = []
direct = ('CAXX_radar_data')
paths = os.listdir(direct)
for path in paths:
    archivos+=1
    matriz = []

    nc = netCDF4.Dataset(direct+'/'+path)
    print(direct+'/'+path)

    nc.variables.keys()

    time = nc.variables['time']
    azimuth = nc.variables['azimuth'][:]
    rangebin = nc.variables['rangebin'][:]
    dbz = nc.variables['rawdBZ'][:]

    time_var = netCDF4.num2date(time[:], time.units)
    for lectura in dbz:
        aux = np.array(lectura)
        aux = np.nan_to_num(aux)
        aux = aux[52:67]
        ma = []
        for i in aux:
            ma.append(i[59:330])
        matriz.append(ma)
        
    np.save('DatosLimitados/'+path, matriz)

    print(archivos)


