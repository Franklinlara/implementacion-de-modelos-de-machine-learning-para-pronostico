import numpy as np 
import matplotlib.pyplot as plt 
import pandas as pd 
import datetime
import pickle
from datetime import datetime as dat
from scipy.io import arff
import datetime as dt
import matplotlib.dates as mdates

print("Recoleccion de datos")

data = pd.read_csv("medianaAllData.csv")
data2 = pd.read_csv("sumaAllData.csv")
data3 = pd.read_csv("mediaAllData.csv")

dates = data['0']
y = data['1']

x = [dt.datetime.strptime(d,'%Y-%m-%d').date() for d in dates]
cont = 0
x_label = [x[0]]
for i in x:
    if (cont == 30):
        x_label.append(i)
        cont = 0
    else:
        x_label.append('')
        cont+=1
x_label[len(x_label)-2] = x[len(x)-1]

x_label[len(x_label)-16] = ''

plt.gca().xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d'))
plt.gca().xaxis.set_major_locator(mdates.DayLocator())
plt.plot(x,y)
plt.xticks(x,x_label, fontsize=16)
plt.yticks(fontsize=16)
plt.axvspan('2015-03-25','2015-05-31',  alpha=0.5)
plt.axvspan('2016-02-01','2016-05-31',  alpha=0.5)                  
plt.axvspan('2017-02-01','2017-04-28',  alpha=0.5)
plt.ylabel("Reflectividad (dBZ)", fontsize=18)
plt.xlabel("Fecha", fontsize=18)
plt.gcf().autofmt_xdate()
plt.show()

print("Terminado")