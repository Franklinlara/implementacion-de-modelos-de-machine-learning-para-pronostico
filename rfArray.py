from sklearn.ensemble import RandomForestRegressor
import numpy as np 
from matplotlib import pyplot
import matplotlib.pyplot as plt 
import pandas as pd 
import datetime
from sklearn import model_selection
from sklearn.linear_model import LogisticRegression
import pickle
from datetime import datetime as dt
from sklearn.datasets import make_regression
from sklearn.model_selection import train_test_split
from scipy.io import arff
from sklearn import preprocessing
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import r2_score
from sklearn.model_selection import RandomizedSearchCV
from scipy.stats import linregress
from sklearn.metrics import accuracy_score


def rfExecute(sizeNEstima):
    pyplot.plot(1,2)
    instanteInicial = dt.now()
    # importando los datos en binario
    print("Recoleccion de datos")
    #data = np.load('DatosML/2057-03-05.npy')

    data = pd.read_csv("dataTraining.csv")

    uno = data["0"]

    data = pd.DataFrame(data)

    del data['Unnamed: 0']

    standardScaler_x = StandardScaler()
    data = standardScaler_x.fit_transform(data)

    print("Data normalizado")
    data = pd.DataFrame(data)

    entrenamiento = data[data.columns[0:14]]
    salida = data[data.columns[14]]

    salida2 = data[data.columns[14]]


    x, x_test, y, y_test = train_test_split(entrenamiento, salida , test_size=0.2)

    print("Datos para entrenamiento")
    print(len(y))
    print("Datos para validar")
    print(len(y_test))

    # Modelo with RF
    print("Modelos Aprendiendo")
    regresion = RandomForestRegressor(n_estimators=sizeNEstima, n_jobs=-1)

    regresion.fit(x, y)

    print("regresion")
    print(regresion)

    print("Validacion")
    y_pred = regresion.predict(x_test)

    print("Almacenando datos normalizados")

    np.savetxt('resultadosMax/Normalizados1RF'+str(sizeNEstima)+'Pred.csv', np.array(y_pred), delimiter=',')
    np.savetxt('resultadosMax/NormalizadosRF'+str(sizeNEstima)+'Real.csv', np.array(y_test), delimiter=',')


    # Error medio cuadratico
    from sklearn.metrics import mean_squared_error

    print("Error medio Cuadratico Lineal")
    print(mean_squared_error(y_test, y_pred))

    print("NSE: ")
    print(r2_score(y_test, y_pred))

    print("PCC")
    print(linregress(y_pred, y_test))
    slope, intercept, r_value, p_value, std_err = linregress(y_test, y_pred)
    print("slope: %f    intercept: %f" % (slope, intercept))
    pyplot.plot(y_test, y_pred, 'ob', label='Datos Originales')
    pyplot.plot(y_test, intercept + slope*y_test, 'r', label='Intercepción')
    pyplot.legend()
    pyplot.savefig("imagenes/PCCRF"+str(sizeNEstima)+"NOR.png", transparent=True, format="png")
    pyplot.clf()

    print("Bias")
    a = y_pred - y_test
    a = sum(a)
    b = sum(y_test)
    print(a/b)

    # Calculos d elos valores para ver el error de otra forma
    errors = abs(y_pred - y_test)

    # Calculate mean absolute percentage error (MAPE)
    mape = 100 * (errors / abs(y_test))

    # Calculate and display accuracy 
    accuracy = 100 - np.mean(mape)
    print("Acurracy sklearn")
    print(regresion.score(x_test, y_test))
    print("varianza")
    print(y_test.var())
    print(y_pred.var())

    #grafico de los datos pronosticados
    print("graficando")
    x  = [a for a in range(len(y_pred))]

    yNor = (abs(y_pred)  - abs(y_test))[:100000]

    ### regresando a valores reales
    print("regresando a valores reales")
    #print(x_test)
    x_test = x_test.reset_index()
    x_test2 = x_test.copy()
    x_test2 = x_test2.reset_index()

    del x_test2['level_0']
    del x_test2['index']
    del x_test['index']

    y_test = y_test.tolist()
    #print(y_test)

    x_test['14'] = pd.Series(y_pred)
    x_test2['14'] = pd.Series(y_test)

    print("valores reales")
    datos2 = standardScaler_x.inverse_transform(x_test)
    datos2 = pd.DataFrame(datos2)

    datos3 = standardScaler_x.inverse_transform(x_test2)
    datos3 = pd.DataFrame(datos3)

    print("Almacenando Matrices")
    pd.DataFrame(datos2).to_csv('resultadosMax/FinalMatrixPronosticadaRF'+str(sizeNEstima)+'.csv')
    pd.DataFrame(datos3).to_csv('resultadosMax/FinalMatrixTestRF'+str(sizeNEstima)+'.csv')

    y_pred =  datos2[datos2.columns[14]]
    y_test =  datos3[datos3.columns[14]]

    # Calcular valores reales
    print("-------Con los valores Reales----------") 
    print("Error medio Cuadratico Lineal")
    print(mean_squared_error(y_test, y_pred))

    print("NSE: ")
    print(r2_score(y_test, y_pred))

    print("PCC")
    print(linregress(y_pred, y_test))
    slope, intercept, r_value, p_value, std_err = linregress(y_test, y_pred)
    print("slope: %f    intercept: %f" % (slope, intercept))
    pyplot.plot(y_test, y_pred, 'ob', label='Datos Originales')
    pyplot.plot(y_test, intercept + slope*y_test, 'r', label='Intercepción')
    pyplot.legend()
    pyplot.savefig("imagenes/Total250PCCRF"+str(sizeNEstima)+"REAL.png", transparent=True, format="png")
    pyplot.clf()

    print("Bias")
    a = y_pred - y_test
    a = sum(a)
    b = sum(y_test)
    print(a/b)

    # Calculos d elos valores para ver el error de otra forma
    errors = abs(y_pred - y_test)

    # Calculate mean absolute percentage error (MAPE)
    mape = 100 * (errors / abs(y_test))

    # Calculate and display accuracy 
    accuracy = 100 - np.mean(mape)
    print("varianza")
    print(y_test.var())
    print(y_pred.var())
    print(y_test.var() - y_pred.var())

    print("Almacenando solo valores de salida") 
    pd.DataFrame(y_pred).to_csv('resultadosMax/FinalRealy_predRF'+str(sizeNEstima)+'.csv')
    pd.DataFrame(y_test).to_csv('resultadosMax/FinalRealy_testRF'+str(sizeNEstima)+'.csv')

    # save the model to disk
    pickle.dump(regresion, open('resultadosMax/FinalPruebaRF'+str(sizeNEstima)+'.sav', 'wb'))

    # al final de la partida
    instanteFinal = dt.now()
    tiempo = instanteFinal - instanteInicial # Devuelve un objeto timedelta
    segundos = tiempo.seconds
    print("Tiempo: "+segundos)

    # conversion z- r
    print("conversion z - R")

    Z_test = 10**(y_test/10)
    Z_pred = 10**(y_pred/10)

    convR = Z_test ** 2.03
    convR = 103*convR
    convT = Z_pred ** 2.03
    convT = 103*convT

    print("Almacenando solo valores de tasa de lluvia") 
    pd.DataFrame(convT).to_csv('resultadosMax/TasaLluviaFinalRealy_predRF'+str(sizeNEstima)+'.csv')
    pd.DataFrame(convR).to_csv('resultadosMax/TasaLluviaFinalRealy_testRF'+str(sizeNEstima)+'.csv')
    

    # Calcular valores reales
    print("-------Con los valores Tasa de lluvia----------") 
    print("NSE: ")
    print(r2_score(convR, convT))

    print("PCC")
    print(linregress(convT, convR))
    slope, intercept, r_value, p_value, std_err = linregress(convR, convT)
    print("slope: %f    intercept: %f" % (slope, intercept))
    pyplot.plot(convR, convT, 'ob', label='Datos Originales')
    pyplot.plot(convR, intercept + slope*convR, 'r', label='Ajuste')
    pyplot.legend()
    pyplot.savefig("imagenes/PCCRF"+str(sizeNEstima)+"TL.png", transparent=True, format="png")
    pyplot.clf()
    
    print("Bias")
    a = convT - convR
    a = sum(a)
    b = sum(convR)
    print(a/b)

    # Calculos d elos valores para ver el error de otra forma
    errors = abs(convT - convR)

    # Print out the mean absolute error (mae)
    print('Mean Absolute Error:', round(np.mean(errors), 5), 'degrees.')

    # Calculate mean absolute percentage error (MAPE)
    mape = 100 * (errors / abs(convR))

    # Calculate and display accuracy 
    accuracy = 100 - np.mean(mape)
    print("varianza")
    print(convR.var())
    print(convT.var())


    features = pd.get_dummies(data)
    feature_list = list(features.columns)

    # Get numerical feature importances
    importances = list(regresion.feature_importances_)

    # List of tuples with variable and importance
    feature_importances = [(feature, round(importance, 2)) for feature, importance in zip(feature_list, importances)]

    # Sort the feature importances by most important first
    feature_importances = sorted(feature_importances, key = lambda x: x[1], reverse = True)

    # Print out the feature and importances 
    [print('Variable: {:20} Importancia: {}'.format(*pair)) for pair in feature_importances];

    pyplot.clf()

    # Import matplotlib for plotting and use magic command for Jupyter Notebooks
    import matplotlib.pyplot as plt

    # Set the style
    pyplot.style.use('fivethirtyeight')
    # list of x locations for plotting
    x_values = list(range(len(importances)))
    # Make a bar chart
    pyplot.bar(x_values, importances, orientation = 'vertical')
    # Tick labels for x axis
    pyplot.xticks(x_values, feature_list, rotation='vertical')
    # Axis labels and title
    pyplot.ylabel('Importancia'); pyplot.xlabel('Variable'); pyplot.title('Importancia de las Variables');
    pyplot.savefig("imagenes/IVRF"+str(sizeNEstima)+"TL.png", transparent=True, format="png")
    pyplot.clf()


tam = [10, 100, 250, 500, 1000]
for i in tam:
    print("TEST OF "+ str(i)) 
    rfExecute(i)
print("Terminado")