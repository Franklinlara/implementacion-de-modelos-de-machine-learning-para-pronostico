import matplotlib.pyplot as plt
x = [10,100,1000,10000,100000]
PCC = [0.60,0.60,0.61,0.0,0.0]
NSE = [0.201,0.33,0.35,0.0,0.0]
RMSE = [7.49,6.83,6.76,0.0,0.0]
BIAS = [0.0038,0.044,0.040,0.0,0.0]
VAR = [72.08,25.01,30.01,0.0,0.0]
DIFF = [1.64,45.48,40.72,0.0,0.0]
a = [32.15,19.86,17.06,0,0]

fig, axs = plt.subplots(2, 2)
plt.title("Valores de criterios de evaluación en los modelos de RF")
axs[0, 0].plot(x, PCC)
axs[0, 0].set_title('PCC')
axs[0, 1].plot(x, NSE)
axs[0, 1].set_title('NSE')
axs[1, 0].plot(x, RMSE)
axs[1, 0].set_title('RMSE')
axs[1, 1].plot(x, BIAS)
axs[1, 1].set_title('BIAS')

for ax in axs.flat:
    ax.set(xlabel='N. Arboles', ylabel='Valores')
    

# Hide x labels and tick labels for top plots and y ticks for right plots.
for ax in axs.flat:
    ax.label_outer()

plt.show()
plt.clf()

plt.plot(x,a)
plt.xlabel("Número de árboles en RF")
plt.ylabel("Accuracy (Exactitud)")
plt.title("Accuracy por número de árboles en los entrenamientos de RF")

plt.show()