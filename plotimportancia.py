import matplotlib.pyplot as plt

fig = plt.figure(u'Importancia de las Variables') # Figure
ax = fig.add_subplot(111) # Axes

nombres = ['azimuth','rangebin','σ t-5','Me t-5','σ t-4','Me t-4','σ t-3','Me t-3','σ t-2','Me t-2','σ t-1','Me t-1' ,'σ t','Me t']
datos = [0,0.06,0.07,0.05,0.06,0.04,0.06,0.04,0.05,0.04,0.05,0.05,0.31,0.11]
xx = range(len(datos))

ax.bar(xx, datos, width=0.5, align='center')
ax.set_xticks(xx)
plt.ylabel("Importancia")
ax.set_xticklabels(nombres, rotation="70")
plt.xlabel("Variables")
plt.title("Importancia de las Variables")
plt.show()