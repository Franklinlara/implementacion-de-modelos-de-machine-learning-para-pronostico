from sklearn.ensemble import RandomForestRegressor
import numpy as np 
import matplotlib.pyplot as plt 
import pandas as pd 
import datetime
from sklearn import model_selection
from sklearn.linear_model import LogisticRegression
import pickle
from datetime import datetime as dt
from sklearn.datasets import make_regression
from sklearn.model_selection import train_test_split
from scipy.io import arff
from sklearn import preprocessing
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import r2_score
from sklearn.model_selection import RandomizedSearchCV
from scipy.stats import linregress
from sklearn.metrics import accuracy_score


instanteInicial = dt.now()
# importando los datos en binario
print("Recoleccion de datos")

data = pd.read_csv("dataTraining.csv")
uno = data["0"]

data = pd.DataFrame(data)

del data['Unnamed: 0']

standardScaler_x = StandardScaler()
data = standardScaler_x.fit_transform(data)

print(len(data))
print("Data normalizado")
data = pd.DataFrame(data)

entrenamiento = data[data.columns[0:14]]
salida = data[data.columns[14]]

salida2 = data[data.columns[14]]

x, x_test, y, y_test = train_test_split(entrenamiento, salida , test_size=0.2)

print("Datos para entrenamiento")
print(len(y))
print("Datos para validar")
print(len(y_test))

pd.DataFrame(y_test).to_csv('y_testRF02Real.csv')

# Modelo with RF
print("Modelos Aprendiendo")

from sklearn.svm import SVR
regresion = SVR(kernel = 'rbf', cache_size=4000)

regresion.fit(x, y)

print("regresion")
print(regresion)

print("Validacion")
y_pred = regresion.predict(x_test)

print("Almacenando datos normalizados")

np.savetxt('Normalizados1RF01Pred.csv', np.array(y_pred), delimiter=',')
np.savetxt('NormalizadosRF01Real.csv', np.array(y_test), delimiter=',')

# Error medio cuadratico
from sklearn.metrics import mean_squared_error

print("Error medio Cuadratico Lineal")
print(mean_squared_error(y_test, y_pred))

print("NSE: ")
print(r2_score(y_test, y_pred))

print("PCC")
print(linregress(y_pred, y_test))
slope, intercept, r_value, p_value, std_err = linregress(y_test, y_pred)
print("slope: %f    intercept: %f" % (slope, intercept))
plt.plot(y_test, y_pred, 'o', label='Datos Originales')
plt.plot(y_test, intercept + slope*y_test, 'r', label='Ajuste')
plt.legend()
plt.savefig("imagenes/PCCRF02NOR.png", transparent=True, format="png")
plt.clf()

print("Bias")
a = y_pred - y_test
a = sum(a)
b = sum(y_test)
print(a/b)

# Calculos d elos valores para ver el error de otra forma
errors = abs(y_pred - y_test)

# Calculate mean absolute percentage error (MAPE)
mape = 100 * (errors / abs(y_test))

# Calculate and display accuracy 
accuracy = 100 - np.mean(mape)
print("Acurracy sklearn")
print(regresion.score(x_test, y_test))

print("graficando")
x  = [a for a in range(len(y_pred))]

yNor = (abs(y_pred)  - abs(y_test))[:100000]

### regresando a valores reales
print("regresando a valores reales")
x_test = x_test.reset_index()
x_test2 = x_test.copy()
x_test2 = x_test2.reset_index()

del x_test2['level_0']
del x_test2['index']
del x_test['index']

y_test = y_test.tolist()

x_test['14'] = pd.Series(y_pred)
x_test2['14'] = pd.Series(y_test)


print("valores reales")
datos2 = standardScaler_x.inverse_transform(x_test)
datos2 = pd.DataFrame(datos2)

datos3 = standardScaler_x.inverse_transform(x_test2)
datos3 = pd.DataFrame(datos3)

print("Almacenando Matrices")
pd.DataFrame(datos2).to_csv('FinalMatrixPronosticadaRF02.csv')
pd.DataFrame(datos3).to_csv('FinalMatrixTestRF02.csv')

y_pred =  datos2[datos2.columns[14]]
y_test =  datos3[datos3.columns[14]]

# Calcular valores reales
print("-------Con los valores Reales----------") 
print("Error medio Cuadratico Lineal")
print(mean_squared_error(y_test, y_pred))

print("NSE: ")
print(r2_score(y_test, y_pred))

print("PCC")
print(linregress(y_pred, y_test))
slope, intercept, r_value, p_value, std_err = linregress(y_test, y_pred)
print("slope: %f    intercept: %f" % (slope, intercept))
plt.plot(y_test, y_pred, 'o', label='Datos Originales')
plt.plot(y_test, intercept + slope*y_test, 'r', label='Ajuste')
plt.legend()
plt.savefig("imagenes/PCCRF02REAL.svg", transparent=True, format="svg")
plt.clf()

print("Bias")
a = y_pred - y_test
a = sum(a)
b = sum(y_test)
print(a/b)

# Calculos d elos valores para ver el error de otra forma
errors = abs(y_pred - y_test)

# Print out the mean absolute error (mae)
print('Mean Absolute Error:', round(np.mean(errors), 5), 'degrees.')

# Calculate mean absolute percentage error (MAPE)
mape = 100 * (errors / abs(y_test))

# Calculate and display accuracy 
accuracy = 100 - np.mean(mape)
print('Accuracy :', round(accuracy, 2), '%.')

print("Almacenando solo valores de salida") 
pd.DataFrame(y_pred).to_csv('FinalRealy_predRF02.csv')
pd.DataFrame(y_test).to_csv('FinalRealy_testRF02.csv')


# save the model to disk
pickle.dump(regresion, open('FinalPruebaRF02.sav', 'wb'))

# al final de la partida
instanteFinal = dt.now()
tiempo = instanteFinal - instanteInicial # Devuelve un objeto timedelta
segundos = tiempo.seconds
print("Tiempo: "+segundos)

print(list(zip(x, regresion.feature_importances_)))

feature_list = list(entrenamiento.columns)

# conversion z- r
print("conversion z - R")

Z_test = 10**(y_test/10)
Z_pred = 10**(y_pred/10)

convR = Z_test ** 2.03
convR = 103*convR

convT = Z_pred ** 2.03
convT = 103*convT

print("Almacenando solo valores de tasa de lluvia") 
pd.DataFrame(convT).to_csv('TasaLluviaFinalRealy_predRF02.csv')
pd.DataFrame(convR).to_csv('TasaLluviaFinalRealy_testRF02.csv')

# Calcular valores reales
print("-------Con los valores Tasa de lluvia----------") 

print("NSE: ")
print(r2_score(convR, convT))

print("PCC")
print(linregress(convT, convR))
slope, intercept, r_value, p_value, std_err = linregress(convR, convT)
print("slope: %f    intercept: %f" % (slope, intercept))
plt.plot(convR, convT, 'o', label='Datos Originales')
plt.plot(convR, intercept + slope*convR, 'r', label='Ajuste')
plt.legend()
plt.savefig("imagenes/PCCRF02TL.svg", transparent=True, format="svg")
plt.clf()

print("Bias")
a = convT - convR
a = sum(a)
b = sum(convR)
print(a/b)

# Calculos d elos valores para ver el error de otra forma
errors = abs(convT - convR)

# Print out the mean absolute error (mae)
print('Mean Absolute Error:', round(np.mean(errors), 5), 'degrees.')

# Calculate mean absolute percentage error (MAPE)
mape = 100 * (errors / abs(convR))

# Calculate and display accuracy 
accuracy = 100 - np.mean(mape)
print('Accuracy :', round(accuracy, 2), '%.')